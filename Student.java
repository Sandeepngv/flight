public class Student {
	int age;
	String name;
	static String course;
	Student(int age, String name, String course){
		this.age=age;
		this.name=name;
		this.course=course;
	}
	public void studentDetails() {
		System.out.println("Age is"+age);
		System.out.println("Name is"+name);
		System.out.println("Course is"+course);
	
	}

	public static void main(String[] args) {
		Student s1=new Student(21,"Sandeep","java");
		Student s2=new Student(45,"Ajay","java");
		Student s3=new Student(31,"Uma","java");
		Student s4=new Student(41,"Krishna","java");
		s1.studentDetails();
		s2.studentDetails();
		s3.studentDetails();
		s4.studentDetails();  
		}

}
